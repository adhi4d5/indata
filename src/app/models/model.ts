export interface userData {
    id: Number;
    name: string;
    address: string;
    age:Number;
    mailId:string;
}
