import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { userData } from '../models/model';
import { environment } from '../../environments/environment';
import { API_ENDPOINTS } from '../models/apiEndpoints';
const data = '../../assets/mockdata/userdata.json';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getList(){
    return this.http.get<userData[]>(`${environment.apiUrl}/${API_ENDPOINTS.API_Table}`);
  }
   getUsersData(){
     return this.http.get(data);
   }
}
