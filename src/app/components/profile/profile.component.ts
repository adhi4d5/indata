import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }
  name = 'Add and show image using angular'

  model: any = {};
  image1: any;

  fileChange(event) {
    if (event.target.files) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.image1 = reader.result;
      };
    }
  }
}

