import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  constructor(public dialog: MatDialog) {}
  data: any = { userName: "", password: "" };
  DialogContentExampleDialog: any = [];

  ngOnInit(): void {}
  search() {
    console.log("searchValues", this.data);
  }
  openDialog() {
    const dialogRef = this.dialog.open(this.DialogContentExampleDialog);

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
