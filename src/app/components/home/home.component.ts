import {AfterViewInit,OnInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { userData } from '../../models/model';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit,AfterViewInit  {
  displayedColumns: string[] = ['id', 'name', 'address'];
  dataSource: MatTableDataSource<userData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  users: userData[] = [];
  showcard: boolean;
  constructor(private ApiService: ApiService) { } 

  ngOnInit(){
    this.getCountries();
  }

  ngAfterViewInit() {
    
  }

  getMoreInformations(data){
    return 'Id : ' + data.id
    + ' \n  name : ' +  data.name
    + ' \n  address : ' +  data.address
    + ' \n  age : ' +  data.age
    + ' \n  mailId : ' +  data.mailId
  }

  getCountries(){
    this.ApiService.getUsersData().subscribe((response: userData[])=>{
      console.log("response",response);
       this.users = response;
       console.log(this.users)
       this.dataSource = new MatTableDataSource(this.users);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
     },(error) => {
      console.log("error",error);
     })
    }
  

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

